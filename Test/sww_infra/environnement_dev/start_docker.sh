#!/bin/bash
cp pre-commit .git/hooks/
chmod +x .git/hooks/pre-commit
export DEVELOPPER=$(whoami)

sudo chown -R $(whoami) $(pwd)/sites/default
if [ ! -d $(pwd)"/sites/default/files" ]; then
    mkdir $(pwd)/sites/default/files
fi
chmod -R 777 $(pwd)/sites/default/files

if [ ! -d $(pwd)"/sites/default/translations" ]; then
    mkdir $(pwd)/sites/default/translations
fi
chmod -R 777 $(pwd)/sites/default/translations

sudo chown -R $(whoami):$(whoami) $(pwd)
setfacl -Rm user:$(whoami):rwx $(pwd)
setfacl -dRm user:$(whoami):rwx $(pwd)
sudo chown -R www-data:www-data $(pwd)

cp   sww_infra/environnement_dev/config/settings.php sites/default/settings.php

docker-compose -f   sww_infra/environnement_dev/environnement-dev.yml down -v
docker-compose -f   sww_infra/environnement_dev/environnement-dev.yml --project-name annuaire-lh-dev  up --build
