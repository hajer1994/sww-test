#!/usr/bin/env bash

cd /var/www/html

while ! nc -z "$MYSQL_HOST" "$MYSQL_PORT"; do
    >&2 echo "Mysql  is unavailable - sleeping";
    sleep 3;
done
>&2 echo "$MYSQL_HOST is up - executing command"

if [ $(mysql -h "$MYSQL_HOST" -N -s -u root -proot -e \
        "select count(*) from information_schema.tables where \
            table_schema='annuaires_lh' and table_name='batch';") -eq 1 ]; then
        echo "Database annuaires_lh exist"
    else
        >&2 echo "Importing data into annuaires_lh"
        mysql -h $MYSQL_HOST -u root -proot annuaires_lh < ./annuaires-lh-infrastructure/environnement_dev/annuaires_lh_init.sql
        >&2 echo "Import annuaires_lh has finished"
    fi

composer install --no-interaction --dev
chmod -R 777 ./vendor

>&2 echo "drush enable maintenance mode"
vendor/bin/drush sset system.maintenance_mode 1
>&2 echo "drush clear cache command"
vendor/bin/drush cache:rebuild --cache-clear
>&2 echo "drush import command"
vendor/bin/drush cim -y --source=./exports/all
>&2 echo "drush update database command"
vendor/bin/drush updb -y
>&2 echo "drush update translation"
vendor/bin/drush locale-check
vendor/bin/drush locale-update
vendor/bin/drush cim -y --source=./exports/all
>&2 echo "drush clear cache command"
vendor/bin/drush cache:rebuild --cache-clear
>&2 echo "drush disable maintenance mode"
vendor/bin/drush sset system.maintenance_mode 0

npm install
chmod -R 777 ./node_modules

/usr/sbin/apache2ctl -D FOREGROUND